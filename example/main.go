package main

import (
	"log"
	"net/http"
	"ratelimiter"
	"time"
)

func handlerPing(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := w.Write([]byte("pong\n"))
	if err != nil {
		log.Println(time.Now(), r.Method, r.RequestURI, r.UserAgent(), err.Error())
	}
	log.Println(time.Now(), r.Method, r.RequestURI, r.UserAgent(), http.StatusOK)
}

func main() {
	http.HandleFunc("/ping", ratelimiter.MiddlewareRater(handlerPing, 10*time.Second, 3, 4))

	log.Println("ping listening on 127.0.0.14, port 8000")
	err := http.ListenAndServe("127.0.0.14:8000", nil)
	if err != nil {
		log.Println("Error starting ping server: ", err)
	}
}
