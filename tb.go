package ratelimiter

import (
	"sync"
	"time"
)

// Tocken bucket algoritm
type TB struct {
	tokens int
	cap    int
	rate   time.Duration
	end    chan struct{}
	m      sync.Mutex
}

func NewTB(refillRate time.Duration, capacity, request int) *TB {
	tb := &TB{
		cap:  capacity,
		rate: refillRate,
		end:  make(chan struct{}),
	}
	go tb.Tokens(request)
	return tb
}

func (tb *TB) Allow() bool {
	tb.m.Lock()
	defer tb.m.Unlock()
	if tb.tokens > 0 {
		tb.tokens--
		return true
	}
	return false
}

func (tb *TB) Tokens(requestPerInterval int) {
	ticker := time.NewTicker(tb.rate)
	defer ticker.Stop()

	for {
		tb.m.Lock()
		if tb.tokens+requestPerInterval <= tb.cap {
			tb.tokens += requestPerInterval
		} else {
			tb.tokens = tb.cap
		}
		tb.m.Unlock()
		select {
		case <-ticker.C:
			continue
		case <-tb.end:
			return
		}
	}
}

func (tb *TB) Stop() {
	close(tb.end)
}
