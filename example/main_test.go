package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	l "ratelimiter"
	"testing"
)

func TestHandler(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(handlerPing))
	defer testServer.Close()
	testClient := testServer.Client()

	resp, err := testClient.Get(testServer.URL)
	if err != nil {
		t.Errorf("Get error: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Errorf("response code is not 200: %d", resp.StatusCode)
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("io.ReadAll error: %v", err)
	}
	if string(data) != "pong\n" {
		t.Error("response body does not equal")
	}
}

func TestMiddlewareRater(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(l.MiddlewareRater(handlerPing, 1, 0, 0)))
	defer testServer.Close()
	testClient := testServer.Client()

	resp, err := testClient.Get(testServer.URL)
	if err != nil {
		t.Errorf("Get error: %v", err)
	}
	if resp.StatusCode != http.StatusTooManyRequests {
		t.Errorf("response code is not 429: %d", resp.StatusCode)
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("io.ReadAll error: %v", err)
	}
	if string(data) != "too many requests\n" {
		t.Error("response body does not equal")
	}
}
