package ratelimiter

import (
	"log"
	"net/http"
	"time"
)

func MiddlewareRater(next func(writer http.ResponseWriter, request *http.Request), t time.Duration, capacity int, requests int) http.HandlerFunc {
	limiter := NewTB(t, capacity, requests)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !limiter.Allow() {
			w.WriteHeader(http.StatusTooManyRequests)
			_, err := w.Write([]byte("too many requests\n"))
			if err != nil {
				log.Println(time.Now(), r.Method, r.RequestURI, r.UserAgent(), err.Error())
			}
			log.Println(time.Now(), r.Method, r.RequestURI, r.UserAgent(), http.StatusTooManyRequests)
			return
		} else {
			next(w, r)
		}
	})
}
